# Kuma - Ansible Role

This role covers deployment, configuration and software updates of Kuma. This role is released under MIT Licence and we give no warranty for this piece of software. Currently supported OS - Debian.

You can deploy test instance using `Vagrantfile` attached to the role.
`vagrant up`

`ansible-playbook -b Playbooks/kuma.yml`

Then you can then access kuma from your computer on `http://192.168.33.44:3001`

The first user you create becomes the admin. To access the admin page, go to `http://192.168.33.44:3001/dashboard`


## Playbook
The playbook includes node role and deploys entire stack needed to run Kuma. Additional role is also available in the Ansible roles repos in git.


## Tags
You can use tags when you deploy:
- `update`: to update kuma


## CHANGELOG
- **08.04.2024** - Role creation